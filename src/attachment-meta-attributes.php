<?php
/**
 * Plugin Name: Attachment Meta Attributes
 * Plugin URI: https://bitbucket.org/MarkyMarkNo1/wp-plugin-attachment-meta-attributes
 * Description: Appends metadata in HTML5 data-* attributes to image attachment links when added to a post.
 * Version: 0.1.0-develop
 * Author: MarkyMarkNo1
 * Author URI: https://bitbucket.org/MarkyMarkNo1
 * License: GPL2
 */


/*
 * apply_filters ( 'image_send_to_editor', string $html, int $id, string $caption, string $title, string $align, string $url, string|array $size, string $alt )
 *
 * https://developer.wordpress.org/reference/hooks/image_send_to_editor/
 */
function add_attachment_metadata( $html, $id, $caption, $title, $align, $url, $size, $alt ) {

  /*
   * Proceed if a link is provided. Might lead to Media File, Attachment Page or
   * Custom URL.
   */
  if ( $url ) {

    /*
     * wp_prepare_attachment_for_js ( $attachment )
     *
     * http://codex.wordpress.org/Function_Reference/wp_prepare_attachment_for_js
     */
    $attachment = wp_prepare_attachment_for_js( $id );

    if ( $attachment ) {

      $html = str_replace( '<a ', '<a ' .
        'data-attachment-id="' . $id . '" ' .
        'data-attachment-title="' . $attachment['title'] . '" ' .
        'data-attachment-filename="' . $attachment['filename'] . '" ' .
        'data-attachment-url="' . $attachment['url'] . '" ' .
        'data-attachment-link="' . $attachment['link'] . '" ' .
        'data-attachment-alt="' . $attachment['alt'] . '" ' .
        // 'data-attachment-author="' . $attachment['author'] . '" ' .
        'data-attachment-description="' . $attachment['description'] . '" ' .
        'data-attachment-caption="' . $attachment['caption'] . '" ' .
        'data-attachment-name="' . $attachment['name'] . '" ' .
        // 'data-attachment-status="' . $attachment['status'] . '" ' .
        // 'data-attachment-uploaded-to="' . $attachment['uploadedTo'] . '" ' .
        // 'data-attachment-date="' . $attachment['date'] . '" ' .
        // 'data-attachment-modified="' . $attachment['modified'] . '" ' .
        // 'data-attachment-menu-order="' . $attachment['menuOrder'] . '" ' .
        // 'data-attachment-mime="' . $attachment['mime'] . '" ' .
        'data-attachment-type="' . $attachment['type'] . '" ' .
        // 'data-attachment-subtype="' . $attachment['subtype'] . '" ' .
        // 'data-attachment-icon="' . $attachment['icon'] . '" ' .
        'data-attachment-date-formatted="' . $attachment['dateFormatted'] . '" ' .
        // 'data-attachment-nonces="' . $attachment['nonces'] . '" ' .
        // 'data-attachment-edit-link="' . $attachment['editLink'] . '" ' .
        // 'data-attachment-sizes="' . $attachment['sizes'] . '" ' .
        'data-attachment-width="' . $attachment['width'] . '" ' .
        'data-attachment-height="' . $attachment['height'] . '" ' .
        // 'data-attachment-file-length="' . $attachment['fileLength'] . '" ' .
        // 'data-attachment-compat="' . $attachment['compat'] . '" ' .
      '', $html );

    }

  }

  return $html;
}

/*
 * add_filter ( string $tag, callable $function_to_add, int $priority = 10, int $accepted_args = 1 )
 *
 * https://developer.wordpress.org/reference/functions/add_filter/
 */
add_filter( 'image_send_to_editor', 'add_attachment_metadata', 10, 8 );



