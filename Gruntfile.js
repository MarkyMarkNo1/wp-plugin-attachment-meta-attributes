'use strict';

module.exports = function (grunt) {

  // Configurable paths
  var config = {
    app: 'attachment-meta-attributes',
    src: 'src',
    dist: 'dist'
  };

  // Display the elapsed execution time of grunt tasks
  // https://www.npmjs.com/package/time-grunt
  require('time-grunt')(grunt);

  // JIT plugin loader for Grunt
  // https://www.npmjs.com/package/jit-grunt
  require('jit-grunt')(grunt);

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    config: config,

    // Validate files with ESLint
    // https://www.npmjs.com/package/grunt-eslint
    eslint: {
      all: [
        'Gruntfile.js'
      ]
    },

    // Clean files and folders
    // https://www.npmjs.com/package/grunt-contrib-clean
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '<%= config.dist %>/*',
            '!<%= config.dist %>/.git*'
          ]
        }]
      }
    },

    // Compress files and folders
    // https://www.npmjs.org/package/grunt-contrib-compress
    compress: {
      dist: {
        options: {
          archive: '<%= config.dist %>/<%= config.app %>.zip',
          mode: 'zip'
        },
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.src %>',
          src: [ '**' ],
          dest: '<%= config.app %>'
        }]
      }
    },

    // Automatic desktop notifications for Grunt errors and warnings
    // https://www.npmjs.com/package/grunt-notify
    notify: {
      build: {
        options: {
          title: 'Build successful',
          message: '\'build\' task done, without errors.'
        }
      }
    }

  });


  grunt.registerTask('default', [
    'build'
  ]);

  grunt.registerTask('build', [
    'newer:eslint',
    'clean',
    'compress',
    'notify:build'
  ]);

};
