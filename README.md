# Attachment Meta Attributes plugin for WordPress #

Appends metadata in HTML5 data-* attributes to image attachment links when added to a post.

## About WordPress ##

* [WordPress Codex](http://codex.wordpress.org/Writing_a_Plugin)
* [WordPress.org](http://wordpress.org/)
